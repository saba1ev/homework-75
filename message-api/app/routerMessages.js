const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const router = express.Router();

router.post('/decode', (req,res)=>{
  const text = Vigenere.Decipher(req.body.password).crypt(req.body.text);
  res.send({decoded: text})
});

router.post('/encode', (req,res)=>{
  const text = Vigenere.Cipher(req.body.password).crypt(req.body.text);
  res.send({encoded: text});
});


module.exports = router;