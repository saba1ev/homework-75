const express = require('express');
const messages = require('./app/routerMessages');
const cors = require('cors');


const app = express();
const port = 8000;
app.use(cors())
app.use(express.json());

app.use('/encoder', messages);

app.listen(port, ()=>{
  console.log('Port: ' + port);
})