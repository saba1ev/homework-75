import React, {Component, Fragment} from 'react';
import {Switch, Route} from 'react-router-dom';

import Messages from "./Container/Messages/Messages";


import './App.css';

class App extends Component {
  render() {
    return (
     <Fragment>
       <Switch>
         <Route path='/' exact component={Messages}/>
         <Route render={()=> <h1>Not Found</h1>}/>
       </Switch>
     </Fragment>
    );
  }
}

export default App;
