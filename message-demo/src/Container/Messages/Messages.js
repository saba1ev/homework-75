import React, {Component, Fragment} from 'react';
import {Button, Col, Container, Form, FormGroup, Input, Label} from "reactstrap";

import './Message.css';
import {connect} from "react-redux";
import {
  decodeChangeHandler, encodeChangeHandler, fetch_post_decode,
  fetch_post_encode,
  passwordChangeHandler
} from "../../store/actions/actions";


class Messages extends Component {

  encodeHandler = () => {
    this.props.fetch_post_encode(this.props.encode, this.props.password);
  };
  decodeHandler = () =>{
    this.props.fetch_post_decode(this.props.decode, this.props.password)
  };


  render() {
    return (
     <Fragment>
       <Form>
         <Container>
           <FormGroup>
             <Col xs="3">
               <Label for="exampleText">Encoded Message</Label>
             </Col>
             <Col xs='6'>
               <Input type="textarea" value={this.props.encode} onChange={this.props.enCoded} name="encode" id="exampleText" />
             </Col>
           </FormGroup>

           <FormGroup>
             <Col xs="3">
               <Label for="exampleText">Password</Label>
             </Col>
             <Col xs='3'>
               <Input name="password" value={this.props.password} onChange={this.props.passwordChangeHandler} id="exampleEmail" placeholder="write password" />
             </Col>
             <Button color="success" onClick={this.decodeHandler}>↑</Button>
             <Button color="info" onClick={this.encodeHandler}>↓</Button>
           </FormGroup>

           <FormGroup>
             <Col xs='3'>
               <Label for="exampleText">Decoded Message</Label>
             </Col>
             <Col xs='6'>
               <Input type="textarea" value={this.props.decode} onChange={this.props.deCoded} name="decode" id="exampleText" />
             </Col>
           </FormGroup>
         </Container>

       </Form>
     </Fragment>
    );
  }
}

const mapStateToProps = (state) =>{
  return{
    encode: state.encode,
    decode: state.decode,
    password: state.password
  }
};
const mapDispatchToProps = (dispatch)=>{
  return{
    enCoded: (event)=> dispatch(encodeChangeHandler(event)),
    deCoded: (event)=> dispatch(decodeChangeHandler(event)),
    passwordChangeHandler: (event) => dispatch(passwordChangeHandler(event)),
    fetch_post_encode: (text, password) => dispatch(fetch_post_encode(text, password)),
    fetch_post_decode: (text, password) => dispatch(fetch_post_decode(text, password))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Messages);