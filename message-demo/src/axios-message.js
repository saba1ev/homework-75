import axios from 'axios';

const message = axios.create({
  baseURL: 'http://localhost:8000/encoder'
});

export default message;