import axios from '../../axios-message'

export const MESSAGE_POST_REQUEST = 'MESSAGE_POST_REQUEST';
export const MESSAGE_POST_SUCCESS = 'MESSAGE_POST_SUCCESS';
export const MESSAGE_POST_FAILURE = 'MESSAGE_POST_FAILURE';
export const PASSWORD_CHANGE_HANDLER = 'PASSWORD_CHANGE_HANDLER';
export const ENCODE_CHANGE_HANDLER = 'ENCODE_CHANGE_HANDLER';
export const DECODE_CHANGE_HANDLER = 'DECODE_CHANGE_HANDLER';
export const DECODE_SUCCESS = 'DECODE_SUCCESS'

export const passwordChangeHandler = (event) => {
  return {type: PASSWORD_CHANGE_HANDLER, password: event.target.value}
};
export const encodeChangeHandler = (event) =>{
  return {type: ENCODE_CHANGE_HANDLER, encode: event.target.value}
};
export const decodeChangeHandler = (event) =>{
  return{type: DECODE_CHANGE_HANDLER, decode: event.target.value}
}

export const post_request = () =>{
  return{type: MESSAGE_POST_REQUEST}
};
export const post_success = (message) =>{
  return{type: MESSAGE_POST_SUCCESS, message}
};

export const decodeSuccess = (code) => {
  return {type: DECODE_SUCCESS, code};
};

export const post_failure = (error) =>{
  return{type: MESSAGE_POST_FAILURE, error}
};

export const fetch_post_encode = (text, password) =>{
  return (dispatch)=>{
    dispatch(post_request());
    axios.post('/encode', {text, password}).then(response=>{
      dispatch(post_success(response.data.encoded))
    }, error =>{
      dispatch(post_failure(error));
    })
  }
};

export const fetch_post_decode = (text, password) =>{
  console.log(text);
  console.log(password);
  return (dispatch)=>{
    dispatch(post_request());
    axios.post('/decode', {text, password}).then(response=>{
      console.log(response.data);
      dispatch(decodeSuccess(response.data.decoded))
    }, error =>{
      dispatch(post_failure(error));
    })
  }
};