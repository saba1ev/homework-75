import {
  DECODE_CHANGE_HANDLER, ENCODE_CHANGE_HANDLER,
  MESSAGE_POST_FAILURE,
  MESSAGE_POST_REQUEST,
  PASSWORD_CHANGE_HANDLER, MESSAGE_POST_SUCCESS, DECODE_SUCCESS
} from "../actions/actions";

const initalState = {
  encode : '',
  password: '',
  decode: ''
};

const reducer = (state = initalState, action) =>{
  switch (action.type) {
    case MESSAGE_POST_REQUEST:
      return{
        ...state,

      };
    case MESSAGE_POST_SUCCESS:
      return {...state, decode: action.message};
    case ENCODE_CHANGE_HANDLER:
      return{
        ...state,
        encode: action.encode
      };
    case DECODE_CHANGE_HANDLER:
      return{
        ...state,
        decode: action.decode
      };
    case MESSAGE_POST_FAILURE:
      return{
        ...state,
        error: action.error
      };
    case PASSWORD_CHANGE_HANDLER:
      return {...state, password: action.password};
    case DECODE_SUCCESS:
      return {...state, encode: action.code}
    default:
      return state
  }
};

export default reducer;